/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta2

import (
	"errors"
	"fmt"
	"os"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/yaml"
)

// KubernetesPodSpecPatchType defines the type of patch allowed to update the Deployment template PodSpec
type KubernetesPodSpecPatchType string

const (
	// PatchTypeJSONPatchType patch strategy uses the JSON Patch specification
	// to give control over the PodSpec objects and arrays to update
	// More info: https://datatracker.ietf.org/doc/html/rfc6902
	PatchTypeJSONPatchType = KubernetesPodSpecPatchType("json")
	// PatchTypeMergePatchType patch strategy applies a key-value replacement on the existing PodSpec
	// More info: https://datatracker.ietf.org/doc/html/rfc7386
	PatchTypeMergePatchType = KubernetesPodSpecPatchType("merge")
	// PatchTypeStrategicMergePatchType patch strategy uses the existing patchStrategy
	// applied to each field of the PodSpec object
	PatchTypeStrategicMergePatchType = KubernetesPodSpecPatchType("strategic")
)

var (
	errPatchConversion = errors.New("converting patch to json")
	errPatchAmbiguous  = errors.New("ambiguous patch: both patch path and patch provided")
	errPatchFileFail   = errors.New("loading patch file")
)

// RunnerSpec defines the desired state of Runner
// +k8s:deepcopy-gen=true
type RunnerSpec struct {
	// The fully qualified domain name for the GitLab instance.
	// For example, https://gitlab.example.com
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="GitLab URL",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text"
	GitLab string `json:"gitlabUrl"`

	//Name of secret containing the 'runner-registration-token' key used to register the runner
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="Registration Token",xDescriptors="urn:alm:descriptor:com.tectonic.ui:selector:core:v1:Secret"
	RegistrationToken string `json:"token"`

	// List of comma separated tags to be applied to the runner
	// More info: https://docs.gitlab.com/ee/ci/runners/#use-tags-to-limit-the-number-of-jobs-using-the-runner
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="Tags",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text"
	Tags string `json:"tags,omitempty"`

	// Option to limit the number of jobs globally that can run concurrently.
	// The operator sets this to 10, if not specified
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="Concurrent",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text"
	Concurrent *int32 `json:"concurrent,omitempty"`

	// Option to define the number of seconds between checks for new jobs.
	// This is set to a default of 30s by operator if not set
	// +operator-sdk:csv:customresourcedefinitions:type=spec,displayName="Check Interval",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text"
	CheckInterval *int32 `json:"interval,omitempty"`

	// Specify whether the runner should be locked to a specific project. Defaults to false.
	Locked bool `json:"locked,omitempty"`

	// Specify if jobs without tags should be run.
	// If not specified, runner will default to true if no tags were specified.
	// In other case it will default to false.
	RunUntagged *bool `json:"runUntagged,omitempty"`

	// Specify whether the runner should only run protected branches. Defaults to false.
	Protected bool `json:"protected,omitempty"`

	// If specified, overrides the default URL used to clone or fetch the Git ref
	CloneURL string `json:"cloneURL,omitempty"`

	// Accepts configmap name. Provides user mechanism to inject environment
	// variables in the GitLab Runner pod via the key value pairs in the ConfigMap
	Environment string `json:"env,omitempty"`

	// If specified, overrides the default GitLab Runner image. Default is the Runner image the operator was bundled with.
	RunnerImage string `json:"runnerImage,omitempty"`

	// If specified, overrides the default GitLab Runner helper image
	HelperImage string `json:"helperImage,omitempty"`

	// The name of the default image to use to run
	// build jobs, when none is specified
	BuildImage string `json:"buildImage,omitempty"`

	// Type of cache used for Runner artifacts
	// Options are: gcs, s3, azure
	// +kubebuilder:validations:Enum=s3;gcs;azure
	CacheType string `json:"cacheType,omitempty"`

	// Path defines the Runner Cache path
	CachePath string `json:"cachePath,omitempty"`

	// Name of tls secret containing the custom certificate
	// authority (CA) certificates
	CertificateAuthority string `json:"ca,omitempty"`

	// Enable sharing of cache between Runners
	CacheShared bool `json:"cacheShared,omitempty"`

	// options used to setup S3
	// object store as GitLab Runner Cache
	S3 *CacheS3Config `json:"s3,omitempty"`
	// options used to setup GCS (Google
	// Container Storage) as GitLab Runner Cache
	GCS *CacheGCSConfig `json:"gcs,omitempty"`
	// options used to setup Azure blob
	// storage as GitLab Runner Cache
	Azure *CacheAzureConfig `json:"azure,omitempty"`

	// allow user to override service account
	// used by GitLab Runner
	ServiceAccount string `json:"serviceaccount,omitempty"`

	// allow user to provide configmap name
	// containing the user provided config.toml
	Configuration string `json:"config,omitempty"`

	// ImagePullPolicy sets the Image pull policy.
	// One of Always, Never, IfNotPresent.
	// Defaults to Always if :latest tag is specified, or IfNotPresent otherwise.
	// More info: https://kubernetes.io/docs/concepts/containers/images#updating-images
	ImagePullPolicy corev1.PullPolicy `json:"imagePullPolicy,omitempty"`

	PodSpec []KubernetesPodSpec `json:"podSpec,omitempty"`
}

// KubernetesPodSpec represents the structure expected when adding a custom PodSpec to configure
// the Pod running the GitLab Runner Manager
type KubernetesPodSpec struct {
	// Name is the name given to the custom Pod Spec
	Name string `json:"name"`
	// Path to the file that defines the changes to apply to the final PodSpec object before it is generated.
	// The file must be a JSON or YAML file.
	// You cannot set the patch_path and patch in the same pod_spec configuration, otherwise an error occurs.
	PatchFile string `json:"patchFile,omitempty"`
	// A JSON or YAML format string that describes the changes which must be applied
	// to the final PodSpec object before it is generated.
	// You cannot set the patch_path and patch in the same pod_spec configuration, otherwise an error occurs.
	Patch string `json:"patch,omitempty"`
	// The strategy the runner uses to apply the specified changes to the PodSpec object generated by GitLab Runner.
	// The accepted values are merge, json, and strategic (default value).
	PatchType KubernetesPodSpecPatchType `json:"patchType"`
}

// JSONMarshal returns the patch data (JSON encoded) and type
func (s *KubernetesPodSpec) JSONMarshal(patchType KubernetesPodSpecPatchType) ([]byte, error) {
	patchBytes := []byte(s.Patch)
	if s.PatchFile != "" {
		if len(s.Patch) > 0 {
			return nil, fmt.Errorf("%w (%s)", errPatchAmbiguous, s.Name)
		}

		var err error
		patchBytes, err = os.ReadFile(s.PatchFile)
		if err != nil {
			return nil, fmt.Errorf("%w (%s): %v", errPatchFileFail, s.Name, err)
		}
	}

	patchBytes, err := yaml.YAMLToJSON(patchBytes)
	if err != nil {
		return nil, fmt.Errorf("%w (%s): %v", errPatchConversion, s.Name, err)
	}

	return patchBytes, nil
}

// CacheS3Config defines options for an S3 compatible cache
type CacheS3Config struct {
	Server string `json:"server,omitempty"`
	// Name of the secret containing the
	// 'accesskey' and 'secretkey' used to access the object storage
	Credentials string `json:"credentials,omitempty"`
	// Name of the bucket in which the cache will be stored
	BucketName string `json:"bucket,omitempty"`
	// Name of the S3 region in use
	BucketLocation string `json:"location,omitempty"`
	// Use insecure connections or HTTP
	Insecure bool `json:"insecure,omitempty"`
}

// CacheGCSConfig defines options for GCS object store
type CacheGCSConfig struct {
	// contains the GCS 'access-id' and 'private-key'
	Credentials string `json:"credentials,omitempty"`
	// Takes GCS credentials file, 'keys.json'
	CredentialsFile string `json:"credentialsFile,omitempty"`
	// Name of the bucket in which the cache will be stored
	BucketName string `json:"bucket,omitempty"`
}

// CacheAzureConfig defines options for Azure object store
type CacheAzureConfig struct {
	// Credentials secret contains 'accountName' and 'privateKey'
	// used to authenticate against Azure blob storage
	Credentials string `json:"credentials,omitempty"`
	// Name of the Azure container in which the cache will be stored
	ContainerName string `json:"container,omitempty"`
	// The domain name of the Azure blob storage
	// e.g. blob.core.windows.net
	StorageDomain string `json:"storageDomain,omitempty"`
}

// RunnerStatus defines the observed state of Runner
type RunnerStatus struct {
	// Reports status of the GitLab Runner instance
	// +operator-sdk:csv:customresourcedefinitions:type=status,displayName="Phase",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text"
	Phase string `json:"phase,omitempty"`

	// Reports status of GitLab Runner registration
	// +operator-sdk:csv:customresourcedefinitions:type=status,displayName="Registration",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text"
	Registration string `json:"registration,omitempty"`

	// Additional information of GitLab Runner registration
	// +operator-sdk:csv:customresourcedefinitions:type=status,displayName="Message",xDescriptors="urn:alm:descriptor:com.tectonic.ui:text"
	Message string `json:"message,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +operator-sdk:csv:customresourcedefinitions:displayName="GitLab Runner"
// +operator-sdk:csv:customresourcedefinitions:resources={{ConfigMap,v1,""},{Secret,v1,""},{Service,v1,""},{Pod,v1,""},{Deployment,v1,""},{PersistentVolumeClaim,v1,""}}

// Runner is the open source project used to run your jobs and send the results back to GitLab
type Runner struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Specification of the desired behavior of a GitLab Runner instance
	Spec RunnerSpec `json:"spec,omitempty"`
	// Most recently observed status of the GitLab Runner.
	// It is read-only to the user
	Status RunnerStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// RunnerList contains a list of Runner
type RunnerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Runner `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Runner{}, &RunnerList{})
}
