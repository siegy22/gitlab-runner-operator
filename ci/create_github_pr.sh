#!/bin/bash

if hub pull-request -m "operator gitlab-runner-operator ($VERSION_NO_PREFIX)" --head="$GITHUB_REPO_NAMESPACE/$GITHUB_PROJECT:$BRANCH" --base="main" 2>&1 | grep "A pull request already exists"; then
    exit 0
else
    exit "${PIPESTATUS[0]}"
fi
